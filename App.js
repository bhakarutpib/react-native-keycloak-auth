/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Button,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';
import {authorize} from 'react-native-app-auth';

const config = {
  warmAndPrefetchChrome: Platform.OS === 'android',
  issuer: Platform.OS === 'android'
      ? 'http://10.2.2.0:8080/auth/realms/my-demo'
      : 'http://localhost:8080/auth/realms/my-demo',
  clientId: 'react-native-app-client',
  redirectUrl: 'io.identityserver.demo:/',
  dangerouslyAllowInsecureHttpRequests: true, // to support android http request
};

const App: () => React$Node = () => {
  const [token, setToken] = React.useState(null);

  const onLogin = async () => {
    try {
      const {accessToken} = await authorize(config);
      console.log(`authorized ${accessToken}`);
      setToken(accessToken);
    } catch (e) {
      console.log(e.toString());
    }
  };

  return (
      <>
        <StatusBar barStyle="dark-content"/>
        <SafeAreaView>
          <ScrollView
              contentInsetAdjustmentBehavior="automatic"
              style={styles.scrollView}>
            <Header/>
            {global.HermesInternal == null ? null : (
                <View style={styles.engine}>
                  <Text style={styles.footer}>Engine: Hermes</Text>
                </View>
            )}
            <View style={styles.body}>
              {token && <Text>{token}</Text>}
              {!token &&
              <Button title="Login" color="#841584" onPress={onLogin}/>}
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
